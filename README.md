# 9JIFS site:

A GIF website using the GIPHY API.

The endpoints used in this app are 'Searching', 'Trending', 'Random', 'Get-Gifs-By-Id'.