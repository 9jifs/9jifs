import { apiKey } from '../utils/constants.js';

export function randomGaming(callbackFn) {
  $.ajax({
    url: 'https://api.giphy.com/v1/gifs/random',
    data: {
      api_key: apiKey,
      tag: 'gaming',
    },
    type: "GET",
    success: callbackFn,
    error: (err) => {
      console.error(err.message);
    },
  });
};
