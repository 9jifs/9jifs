import { apiKey } from '../utils/constants.js';
import { q } from '../utils/query-selector.js';
import { saveUploaded } from '../utils/uploaded.js';

import {previewGifUpload} from '../views/preview-gif-upload.js';

export const upload = async()=>{

  previewGifUpload();

  q('#submit-gif-button').addEventListener('click', async (e) => {

    e.preventDefault();
    const form = q('#upload-form');
    const formData = new FormData(form);
    formData.append('api_key', apiKey);
    
    saveUploaded(formData.get('file'));

    if(formData.get('file').name==='') return alert('Please select your gif.');
    if(formData.get('file').type !== "image/gif") return alert('Please upload the correct file format.');

    const res = await fetch('https://upload.giphy.com/v1/gifs', {
      method: 'POST',
      body: formData,
    });

    if(res.ok){
      q('#preview-default').style.display =null;
      q('#preview-gif').style.display = null;
      q('#preview-gif').setAttribute('src', '');
      
      return alert('Your gif has been uploaded!');
    }
    
  });
  q("#gif-file").value = '';
};
