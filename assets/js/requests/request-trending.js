import { limit, apiKey }  from '../utils/constants.js';

export function trending(callbackFn, offset = 0) {
  $.ajax({
    url: 'https://api.giphy.com/v1/gifs/trending',
    data: {
      api_key: apiKey,
      limit: limit,
      offset: offset,
    },
    type: "GET",
    success: callbackFn,
    error: (err) => {
      console.error(err.message);
    },
  });
};
