import { apiKey, limit } from "../utils/constants.js";

export function requestGifDetails(gifId, callbackfn){
  $.ajax({
    url: `https://api.giphy.com/v1/gifs/${gifId}`,
    data: {
      api_key: apiKey,
      limit: limit,
    },
    type: "GET",
    success: callbackfn,
    error: (err) => {
      console.error(err.message);
    },
  });
};
