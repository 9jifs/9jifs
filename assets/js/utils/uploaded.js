export function saveUploaded(file) {
  const uploaded = localStorage.getItem('uploaded') ? JSON.parse(localStorage.getItem('uploaded')) : [];

  const reader = new FileReader();
  reader.readAsDataURL(file);
  reader.addEventListener('load', () => {
    uploaded.push(reader.result)
    localStorage.setItem('uploaded', JSON.stringify(uploaded))
  });
};
