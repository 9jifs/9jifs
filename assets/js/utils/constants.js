export const limit = 14;
export const apiKey = 'm5F78NPA2kF5cxWayH0IABUUyfjJJ98B';
export const MAXIMUM_OFFSET_VALUE = 1800;
export const FULL_HEART = '❤';
export const EMPTY_HEART = '♡';
export const type = {
  2: 'tall',
  3: 'wide',
  5: 'tall',
  6: 'big',
  8: 'wide',
  9: 'big',
  10: 'tall',
};