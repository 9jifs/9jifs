import { q } from './query-selector.js';

//page
export function setPage(page) {
  q('.root').page = page;
};

export function getPage() {
  return q('.root').page;
};

//search
export function setSearch(search) {
  q('.root').search = search;
};

export function getSearch() {
  return q('.root').search;
};

//offset
export function getOffset() {
  return q('.root').offset;
};

export function resetOffset() {
  q('.root').offset = 0;
};

export function incrementOffset() {
  q('.root').offset = Number(getOffset()) + 10;
};
