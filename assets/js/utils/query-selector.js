export const q = selector => document.querySelector(selector);
export const qs = selector => Array.from(document.querySelectorAll(selector));