export function addFavorite(id, url) {
  const favorites = localStorage.getItem('favorites')? JSON.parse(localStorage.getItem('favorites')): {};
  favorites[id] = url;
  localStorage.setItem('favorites', JSON.stringify(favorites));
};

export function removeFavorite(id) {
  const favorites = JSON.parse(localStorage.getItem('favorites'));
  delete favorites[id];
  localStorage.setItem('favorites', JSON.stringify(favorites));
};

export function resetFavorites() {
  localStorage.setItem('favorites', JSON.stringify({}));
};

export function getFavorites() {
  if (!localStorage.length) return [];
  const favorites = JSON.parse(localStorage.getItem('favorites'));
  return favorites;
};
