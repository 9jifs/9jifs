// local storage
import { addFavorite, removeFavorite } from './utils/favorites.js';
// HTML storage
import { setPage, getPage } from './utils/navigation.js';
import { setSearch, getSearch } from './utils/navigation.js';
import { resetOffset, incrementOffset, getOffset } from './utils/navigation.js';
// requests
import { search } from './requests/request-search.js';
import { requestGifDetails } from './requests/request-by-id.js';
import { trending } from './requests/request-trending.js';
import { upload } from './requests/upload.js';
// views
import { display, displayNext } from './views/view-gifs.js';
import { displayHome } from './views/view-home.js';
import { detailedView } from './views/view-gif-details.js';
import { displayFavorites } from './views/view-favorites.js';
import { q } from './utils/query-selector.js';


document.addEventListener('DOMContentLoaded', () => {

  // home
  setPage('home');
  resetOffset();
  displayHome()
  trending(displayNext)
  incrementOffset();

  document.addEventListener('keypress', (event) => {
    if (event.target.id === 'search') {
      if (event.key === 'Enter') {
        resetOffset();
        setPage('search');
        incrementOffset();
        const encodedSearchQuery = encodeURIComponent(event.target.value);
        setSearch(encodedSearchQuery);
        q('.header-search').value = '';
        search(getSearch(), display);
      }
    }
  });

  document.addEventListener('click', (event) => {

    if (event.target.id === 'home-button') {
      event.preventDefault();
      setPage('home');
      resetOffset();
      displayHome();
      trending(displayNext);
      incrementOffset();
    }

    if (event.target.id === 'home-trending-button') {
      event.preventDefault();
      setPage('trending');
      resetOffset();
      trending(display);
      incrementOffset();
    }

    if (event.target.id === 'trending-button') {
      event.preventDefault();
      setPage('trending');
      resetOffset();
      trending(display);
      incrementOffset();
    }

    if (event.target.id === 'upload-button') {
      event.preventDefault();
      setPage('upload');
      upload();
    }

    if (event.target.id === 'favorites-button') {
      event.preventDefault();
      setPage('favorites');
      displayFavorites();
    }

    if (event.target.classList.contains('img-fluid')) {
      setPage('details');
      requestGifDetails(event.target.getAttribute('data-id'), detailedView);
    }

    if (event.target.classList.contains('add-favorite-button')) {
      event.preventDefault();
      const id = event.target.getAttribute('gif-id');
      const url = event.target.getAttribute('gif-url');

      addFavorite(id, url);

      event.target.className = 'remove-favorite-button';
      event.target.src = './assets/img/FULL_HEART.png';

    } else if (event.target.classList.contains('remove-favorite-button')) {
      event.preventDefault();
      const id = event.target.getAttribute('gif-id');

      removeFavorite(id);

      event.target.className = 'add-favorite-button';
      event.target.src = './assets/img/EMPTY_HEART.png';
    }
  });

  window.addEventListener('scroll', () => {
    const height = document.documentElement.scrollHeight - window.innerHeight;
    const pos = window.scrollY;
    if (pos >= height - 20) {
      const page = getPage();
      if (page === 'trending') {
        incrementOffset();
        trending(displayNext, getOffset());
      } else if (page === 'search') {
        search(getSearch(), displayNext, getOffset());
        incrementOffset();
      } else {
        // if we want to add inf scroll to other pages
      } 
    }
  });
});
