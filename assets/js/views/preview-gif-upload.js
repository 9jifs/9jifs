import { q } from "../utils/query-selector.js";
import { showUpload } from "./view-upload.js";

export const previewGifUpload = () => {

  showUpload();

  q('#gif-file').addEventListener('change', function(){
    const file = this.files[0];

    if(file){
      const reader = new FileReader();

      q('#preview-default').style.display ='none';
      q('#preview-gif').style.display ='block';

      reader.addEventListener('load', function(){
        q('#preview-gif').setAttribute('src', this.result);
      });
      
      reader.readAsDataURL(file);
    }else{
      q('#preview-default').style.display =null;
      q('#preview-gif').style.display = null;
      q('#preview-gif').setAttribute('src', '');
    }
});
}