import { q } from "../utils/query-selector.js"

export const showUpload = () => {
  q(".root").innerHTML = `
  <div id="upload-form-outer">
    <div id="upload-form-view">
      <form id="upload-form">
        <h2 id="upload-heading">Upload your gif</h2>
        <input type="file" name="file" id="gif-file">
        <div class="gif-preview-container">
          <img src="" alt="Gif preview" id="preview-gif">
          <span id="preview-default">Gif preview</span>
        </div>
        <button type="submit" id="submit-gif-button">Submit</button>
      </form>
    </div>
  </div>`;
};
