import { q } from "../utils/query-selector.js"

export async function displayHome() {

  q('.root').innerHTML = `
    <div style="text-align: center">
      <h1>Welcome to the 9JIFS site</h1>
      <h3>View more <a id="home-trending-button">Trending</a> gifs</h3>
      <br>
    </div>`;
};
