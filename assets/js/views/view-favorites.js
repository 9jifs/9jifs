import { q } from '../utils/query-selector.js';
import { getFavorites } from '../utils/favorites.js';
import { randomGaming } from '../requests/request-random.js';
import { detailedView } from './view-gif-details.js';

export function displayFavorites() {

  const favorites = getFavorites();
  const content = [];
  
  for (const id in favorites) {
    content.push(`
    <div class="col">
      <img src="${favorites[id]}" data-id="${id}" class="img-fluid favorite-image">
      <img class="remove-favorite-button" id="heart" gif-id="${id}" gif-url="${favorites[id]}" src="./assets/img/FULL_HEART.png">
    </div>`);
  }

  if (content.length) {
    q('.root').innerHTML = `${content.join('')}`;
  } else {
    q('.root').innerHTML = `${randomGaming(detailedView)}`;
  }
};
