import { q } from '../utils/query-selector.js';
import { getFavorites } from '../utils/favorites.js';
import { type } from '../utils/constants.js';

export async function display(data) {
  const favorites = getFavorites();
  
  const gifs = data.data.map((element, i) => {

    const elementType = type[i] ? type[i] : "";
    const image = element.images.downsized.url ? element.images.downsized.url : element.images.original.url;

    return (`
      <div class="${elementType}" id="masonry">
        <img src="${image}" data-id="${element.id}"class="img-fluid">
        <img class="${favorites[element.id] ? 'remove':'add'}-favorite-button" id="heart" gif-id="${element.id}" gif-url="${image}" src="${favorites[element.id]? './assets/img/FULL_HEART.png':'./assets/img/EMPTY_HEART.png'}">
      </div>`);    
  });

  q('.root').innerHTML = `<div class="grid-wrapper">${gifs.join('')}</div>`;
};

export async function displayNext(data) {
  const favorites = getFavorites();
  
  const gifs = data.data.map((element, i) => {

    const elementType = type[i]? type[i]: "";
    const image = element.images.downsized.url? element.images.downsized.url: element.images.original.url;

    return (`
      <div class="${elementType}" id="masonry">
        <img src="${image}" data-id="${element.id}"class="img-fluid">
        <img class="${favorites[element.id]? 'remove':'add'}-favorite-button" id="heart" gif-id="${element.id}" gif-url="${image}" src="${favorites[element.id]? './assets/img/FULL_HEART.png':'./assets/img/EMPTY_HEART.png'}">
      </div>`);    
  });

  q('.root').innerHTML += `<div class="grid-wrapper">${gifs.join('')}</div>`;
};
