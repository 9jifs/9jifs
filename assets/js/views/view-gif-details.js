import { q } from "../utils/query-selector.js"

export const detailedView = (gif) => {
  q('.root').innerHTML =  `
  <div class="card" style="background: url(${gif.data.images.original.url}) center center; background-size: cover; background-repeat: no-repeat; overflow: hidden">
    <div class="card__profile">
      <img src="${gif.data.user ? gif.data.user.avatar_url : 'https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973461_960_720.png'}" alt="people"/>
      <div class="card__profile__text">
        <h2>${gif.data.title.split('GIF')[0]}</h2>
        <p>by <b>${gif.data.user ? gif.data.user.username : 'Guest user'}</b></p>
      </div>
    </div>
  </div>`
};